FROM golang:latest
MAINTAINER s4ntos "@s4ntos"

# TEST
ENV TYPE test
#ENV DBIMPORT github.com/mattn/go-sqlite3
#ENV DBDRIVER sqlite3
#ENV DBSPEC :memory:

# PROD
#ENV TYPE prod
#ENV DBIMPORT github.com/ziutek/mymysql/mysql
#ENV DBDRIVER mymysql
#ENV DBSPEC "tcp:localhost:3306*baseapp/user/pass"

WORKDIR /go/
RUN go get -v -u github.com/revel/cmd/revel
RUN go get gitlab.com/s4ntos/remote
ADD bin/start.sh bin/start.sh
ENTRYPOINT [ "/go/bin/start.sh" ]