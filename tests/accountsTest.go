package tests

import (
	"net/url"

	"github.com/revel/revel/testing"
	"gitlab.com/s4ntos/remote/app/routes"
)

type AccountsTest struct {
	testing.TestSuite
}

func (t *AccountsTest) Before() {
	// Runs before each test below is executed
}

func (t *AccountsTest) After() {
	// Runs after each test below is executed
	t.Get(routes.Account.Logout())
}

func (t *AccountsTest) TestLoginSuccess_ByEmail() {
	loginData := url.Values{}
	loginData.Add("account", "admin@demo.com")
	loginData.Add("password", "adminpassword")
	loginData.Add("remember", "0")

	t.PostForm("/account/login", loginData)

	// Part of login information (after redirect to profile page)
	t.AssertOk()
	t.AssertContains("Welcome back, Admin User")

	// Do Logout
	t.Get(routes.Account.Logout())
}

func (t *AccountsTest) TestLoginSuccess_ByUsername() {
	loginData := url.Values{}
	loginData.Add("account", "admin")
	loginData.Add("password", "adminpassword")
	loginData.Add("remember", "0")

	t.PostForm("/account/login", loginData)

	// Part of login information (after redirect to profile page)
	t.AssertOk()
	t.AssertContains("Welcome back, Admin User")

	// Do Logout
	t.Get(routes.Account.Logout())
}

func (t *AccountsTest) TestLogoutSuccess() {
	// Log in
	loginData := url.Values{}
	loginData.Add("account", "demo@demo.com")
	loginData.Add("password", "demouser")
	loginData.Add("remember", "0")

	t.PostForm("/account/login", loginData)

	// Log out
	t.Get(routes.Account.Logout())

	t.AssertOk()
	t.AssertContains("You have been successfully logged out")
}

func (t *AccountsTest) TestLoginFail_EmptyEmail() {
	loginData := url.Values{}
	loginData.Add("account", "")
	loginData.Add("password", "demouser")
	loginData.Add("remember", "0")

	t.PostForm("/account/login", loginData)

	t.AssertOk()
	t.AssertContains("Sign In failed")
}

func (t *AccountsTest) TestLoginFail_EmptyPassword() {
	loginData := url.Values{}
	loginData.Add("account", "demo@demo.com")
	loginData.Add("password", "")
	loginData.Add("remember", "0")

	t.PostForm("/account/login", loginData)

	t.AssertOk()
	t.AssertContains("Sign In failed")
}
